=== Ezcape Video Chat (Formally kageshi) ===

Contributors: ruddernationdesigns
Donate link: https://www.paypal.me/RuddernationDesigns
Tags: ezcapechat, video chat, wordpress video chat, flash video chat, chat, kageshi
Requires at least: 4.8
Tested up to: 4.9.6
Stable tag: 1.0.8
License: GNUv3
License URI: https://www.gnu.org/licenses/gpl-3.0.en.html

== Description ==

This allows you to use ezcape chat on your blog! Provided you have flash allowed and you will have to make sure that the chat exists otherwise it wont work!
Use ruddernation for example.

== Installation ==

This will automatically create the page and install the short code with link *domain name/ezcapechat*, If it does not then please read the FAQ.

Simply use shortcode [ezcapechat_page] in a page.

== Screenshots ==

* This shows you the chat form.

* After you've entered a chat room name it'll load but only if the room exists.

* Once loaded the chat room will load like this, but make sure you have flash player allowed on your website!

== Notes ==

You will need to allow 'Flash Player' on your domain, To download please use https://get.adobe.com/flashplayer/.

== Frequently Asked Questions ==

* Q. Can I use this if I'm not logged in?

* A. Yes, unless the system Administrator changes the settings.

* Q. How do I add it to my blog/website?

* A. Just go to the backend and on appearance select menus, From there you can add your page, It'll be *ezcapechat* by default and use shortcode [ezcapechat_page].

* Q. The chat is not loading for me.

* A. Check to see if you have the Adobe flash player installed (http://helpx.adobe.com/flash-player.html) and JavaScript enabled in your browser.

== Changelog ==

= 1.0.1 =

* First version.

= 1.0.2 =

* Name change from Ezcape Chat to Ezcape Video Chat.

= 1.0.3 =

* Added popular rooms and user information.

= 1.0.4 =

* Updated the name to include (kageshi).

= 1.0.5 =

* Added a little JQuery to show/hide the info on the chat page, This also includes my JQuery file.

= 1.0.6 =

* Tested up to new versions of WP and BP.

= 1.0.7 =

* Minor update to the iframe, Thanks to Matteo for the suggestion.

== Social Sites ==

* Ezcape Chat - https://www.ezcapechat.cf/

* Developers Website - https://www.ruddernation.com

* Facebook Page - https://www.facebook.com/rndtinychat

* GitHub - https://github.com/Ruddernation-Designs

* WordPress - https://profiles.wordpress.org/ruddernationdesigns
